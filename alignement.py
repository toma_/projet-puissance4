################################################################################
# Votre travail consiste à compléter les fonctions ci-dessous afin de          #
#  contribuer à la détection d'une victoire                                    #
################################################################################
# Il s'agit bien sûr de respecter scrupuleusement les specifications.
# Un commentaire pour les lignes de code non triviales est exigé.
# Pour tester votre script, il suffit d'exécuter le programme JeuPuissance4.
# Le jeu se joue avec la souris uniquement, les joueurs se la passe à tour de role.
# Si vos fonctions sont justes alors le jeu fonctionnera !
# Bon courage.

def alignement(grille):
    """Fonction qui détecte un alignement de 4 pions dans la grille.
    FONCTIONNEMENT :
        Pour chaque couleur de pion:
            Pour chaque case de la grille:
                Si pion est de la bonne couleur:
                    Ajout au dictionnaire des pions de cette couleur.
            Définition de la liste 'positions' qui contient les formules pour calculer les coordonnées des cases environnantes à un rayon 'rayon' d'un pion 'pion' de coordonnées (y,x).
            Pour chaque coordonnée de pion du dictionnaire des pions de cette couleur:
                Pour chaque formule de calcul des positions envionnantes dans la liste des formules:
                    Pour un rayon allant de 1 à 3:
                        Si la case environnante contient un pion de cette couleur:
                            Augmenter le rayon.
                            Si le rayon vaut 3:
                                Renvoyer Vrai.
                        Sinon:
                            break.

        Renvoyer Faux

    >>> print(alignement([[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0]]))
    False
    >>> print(alignement([[1,1,1,1,1,1],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0]]))
    True
    >>> print(alignement([[1,0,0,0,0,0],[0,1,0,0,0,0],[0,0,1,0,0,0],[0,0,0,1,0,0],[0,0,0,0,1,0],[0,0,0,0,0,1]]))
    True
    >>> print(alignement([[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0]]))
    False
    >>> print(alignement([[3,3,3,3,3,3],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0]]))
    True
    >>> print(alignement([[3,0,0,0,0,0],[0,3,0,0,0,0],[0,0,3,0,0,0],[0,0,0,3,0,0],[0,0,0,0,3,0],[0,0,0,0,0,3]]))
    True
    """
    #####DEBUT PIONS ROUGES####1
    pions_rouges = {}
    for position_y in range(6):
        for position_x in range(6):
            if grille[position_y][position_x]==1:
                pions_rouges["pion"+str(len(pions_rouges)+1)]=(position_y,position_x)
    positions = ['(pion[0]-rayon,pion[1]-rayon)', '(pion[0]-rayon,pion[1])', '(pion[0]-rayon,pion[1]+rayon)','(pion[0],pion[1]-rayon)', '(pion[0],pion[1]+rayon)','(pion[0]+rayon,pion[1]-rayon)','(pion[0]+rayon,pion[1])','(pion[0]+rayon,pion[1]+rayon)']
    for pion in pions_rouges.values():
        for position in positions:
            for rayon in range(1,4):
                pion_y, pion_x = eval(position)
                if (pion_y, pion_x) in pions_rouges.values():
                    if rayon==3:
                        return True
                else:
                    break
    ######FIN PIONS ROUGES#####
    ###########################
    ####DEBUT PIONS JAUNES#####
    pions_jaunes = {}
    for position_y in range(6):
        for position_x in range(6):
            if grille[position_y][position_x]==3:
                pions_jaunes["pion"+str(len(pions_jaunes)+1)]=(position_y,position_x)
    positions = ['(pion[0]-rayon,pion[1]-rayon)', '(pion[0]-rayon,pion[1])', '(pion[0]-rayon,pion[1]+rayon)','(pion[0],pion[1]-rayon)', '(pion[0],pion[1]+rayon)','(pion[0]+rayon,pion[1]-rayon)','(pion[0]+rayon,pion[1])','(pion[0]+rayon,pion[1]+rayon)']
    for pion in pions_jaunes.values():
        for position in positions:
            for rayon in range(1,4):
                pion_y, pion_x = eval(position)
                if (pion_y, pion_x) in pions_jaunes.values():
                    if rayon==3:
                        return True
                else:
                    break
    #####FIN PIONS JAUNES######
    return False

if __name__ == "__main__":
    import doctest
    doctest.testmod()